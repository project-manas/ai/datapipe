import queue as q
import threading

import tensorflow as tf
from tensorflow.python.ops.data_flow_ops import StagingArea

# TODO Include name of queue
from sandblox import TFObject


class Queue(TFObject):
	def __init__(self, shapes, dtypes, capacity, scope_name: str = None, shared_name: str = None):
		self._shapes = shapes
		self._dtypes = dtypes
		self._capacity = capacity
		self.shared_name = shared_name
		super(Queue, self).__init__(scope_name)

	def enqueue(self, values, name=None):
		raise NotImplementedError("Queue.enqueue")

	def dequeue(self, name=None):
		raise NotImplementedError("Queue.dequeue")

	def close(self, cancel_pending_enqueues, name=None):
		raise NotImplementedError("Queue.close")

	def size(self, name=None):
		raise NotImplementedError("Queue.size")

	def dequeue_many(self, n, name=None):
		raise NotImplementedError("Queue.dequeue_many")

	def enqueue_many(self, values, parallel_if_supported, name=None):
		"""Enqueue multiple values in a single request
		:param values: The list of values to enqueue
		:param parallel_if_supported: Some Queue implementations support parallel enqueue of all the values at the expense of guaranteeing enqueue order
		:param name: Name of the operation
		:return:
		"""
		raise NotImplementedError("Queue.enqueue_many")

	def dequeue_up_to(self, n, name=None):
		raise NotImplementedError("Queue.dequeue_up_to")

	@property
	def shapes(self):
		return self._shapes

	@property
	def dtypes(self):
		return self._dtypes

	@property
	def capacity(self):
		return self._capacity


class FIFOQueue(Queue):
	def __init__(self, shapes, dtypes, capacity, scope_name: str = None, shared_name: str = None):
		# TODO Use shapes and dtypes to assert correctness in enqueue, dequeue etc.
		super(FIFOQueue, self).__init__(shapes, dtypes, capacity, scope_name, shared_name)
		self.ds = q.Queue(maxsize=capacity)

	def enqueue(self, values, name=None):
		return lambda v: self.ds.put(v)

	def dequeue(self, name=None):
		return self.ds.get

	def dequeue_many(self, n, name=None):
		raise NotImplementedError("FIFOQueue does not currently support dequeue_many")

	def dequeue_up_to(self, n, name=None):
		raise NotImplementedError("FIFOQueue does not currently support dequeue_up_to")

	def enqueue_many(self, values, parallel_if_supported=False, name=None):
		return self._enqueue_many

	def _enqueue_many(self, values):
		put = self.ds.put
		return [put(item) for item in zip(*values)]

	def close(self, cancel_pending_enqueues, name=None):
		raise NotImplementedError("FIFOQueue does not currently support close")

	def size(self, name=None):
		return self.ds.qsize


class TfFIFOQueue(Queue):
	def __init__(self, shapes, dtypes, capacity, names: dict = None, shared_name: str = None, scope_name: str = None):
		super(TfFIFOQueue, self).__init__(shapes, dtypes, capacity, scope_name, shared_name)
		self.ds = tf.FIFOQueue(capacity, dtypes, shapes, names, shared_name, self.scope.rel)

	@property
	def shapes(self):
		return self.ds.shapes

	@property
	def dtypes(self):
		return self.ds.dtypes

	def enqueue(self, values, name=None):
		return self.ds.enqueue(values, name)

	def dequeue(self, name=None):
		return self.ds.dequeue(name)

	def dequeue_many(self, n, name=None):
		return self.ds.dequeue_many(n, name)

	def dequeue_up_to(self, n, name=None):
		return self.ds.dequeue_up_to(n, name)

	def enqueue_many(self, values, parallel_if_supported=False, name=None):
		return self.ds.enqueue_many(values, name)

	def close(self, cancel_pending_enqueues, name=None):
		return self.ds.close(cancel_pending_enqueues, name)

	def size(self, name=None):
		return self.ds.size(name)


class StagingAreaQueue(Queue):
	def __init__(self, shapes, dtypes, capacity, names: dict = None, shared_name: str = None, scope_name: str = None):
		self.setup_scope(scope_name)
		if shared_name is None:
			self.scope.make_unique()
			shared_name = self.scope.rel
		super(StagingAreaQueue, self).__init__(shapes, dtypes, capacity, shared_name)
		self.ds = StagingArea(dtypes, shapes, names, shared_name, capacity=capacity)

	@property
	def shapes(self):
		return self.ds.shapes

	@property
	def dtypes(self):
		return self.ds.dtypes

	def enqueue(self, values, name=None):
		return self.ds.put(values, name)

	def dequeue(self, name=None):
		return self.ds.get(name)

	def dequeue_many(self, n: int, name=None):
		raise NotImplementedError("Good luck implementing this")

	# TODO I gave up trying
	# TODO Optimize the living shit out of this
	# dequeues = [self.dequeue() for _ in range(n)]
	# return tf.cond(tf.greater_equal(n, self.size()), lambda: dequeues, tf.no_op())

	# dequeues = [self.dequeue()]*n
	# dequeues = list(map (list, zip(*dequeues)))
	# stacked_ops = [tf.stack(op) for op in dequeues]
	# return stacked_ops

	# get_ops = []
	# c = lambda i, get: tf.greater_equal(i, 0)
	# b = lambda i, get: [i-1, self.staging_area.get()]
	# r = tf.while_loop(c, b, [n-1, self.staging_area.get()])
	# with tf.control_dependencies(r):
	# 	return tf.stack(get_ops)
	# return tf.stack([self.staging_area.get() for _ in range(n)])

	# return self.queue.dequeue_many(n, name)

	def dequeue_up_to(self, n, name=None):
		raise NotImplementedError("StagingAreaQueue does not support dequeue_up_to")

	def enqueue_many(self, values, parallel_if_supported=False, name=None):
		raise NotImplementedError("StagingAreaQueue does not support enqueue_many")

	# with tf.variable_scope(name, default_name='enqueue_many'):
	# 	stack_op = tf.parallel_stack if parallel_if_supported else tf.stack
	# 	component_stacks = []
	# 	# return tuple([stack_op([self.staging_area.put(item) for item in component_items])
	# 	# 			  for component_items in tf.unstack(component)])
	# 	# return tuple([stack_op([self.staging_area.get(item) for item in tf.unstack(component)]) for component in values])
	# 	# for component in values:
	# 	# 	component_items = tf.unstack(component)
	# 	# 	enqueue_component_stack = stack_op([self.staging_area.put(item) for item in component_items])
	# 	# 	component_stacks.append(enqueue_component_stack)
	# 	# return tuple(component_stacks)
	# 	tuple_list = list(zip(*[tf.unstack(value) for value in values]))
	# 	return [self.staging_area.put(tup) for tup in tuple_list]
	# # return stack_op([self.staging_area.put(value) for value in tf.unstack(values)])

	def close(self, cancel_pending_enqueues, name=None):
		# TODO Test if a single clear will cancel pending enqueues
		clear_op = self.ds.clear(name)
		return clear_op

	# size_op = self.staging_area.size()
	# c = lambda i: tf.greater(size_op, 0)
	# b = lambda i: clear_op
	# clear_while_not_empty = tf.while_loop(c, b, [tf.constant(0)])
	# return clear_while_not_empty

	def size(self, name=None):
		return self.ds.size()


class HeteroThreadHeteroQueue(TFObject):
	THREAD_STOP_MODES = ['stop', 'destroy']

	class ThreadHandler(object):
		# TODO Add callback for teardown
		def __init__(self, on_build_graph, on_start, *args, **kwargs):
			self._build_results = None
			self._on_build_graph = on_build_graph
			self._on_start = on_start
			self._thread = None
			self.already_started = False
			self._should_stop = False
			self.multiqueue_requested_stop = lambda: False
			self.args = args
			self.kwargs = kwargs

		# signature = inspect.signature(on_build_graph)
		# n_args = len(args)
		# n_params = len(signature.parameters)
		# if n_params == n_args + 1:
		# 	raise ValueError(
		# 		"Cannot bind the provided arguments(%d) to the on_build_graph callback (%d parameters)" % (
		# 			n_args + 1, n_params))

		@property
		def thread(self):
			return self._thread

		def should_stop(self):
			return self._should_stop or self.multiqueue_requested_stop()

		def request_stop(self):
			self._should_stop = True

		def clear_stop(self):
			self._should_stop = False

		def build_graph(self, thread_index: int):
			self._build_results = self._on_build_graph(thread_index, self, *self.args, **self.kwargs)
			try:
				iter(self._build_results)
			except TypeError:
				self._build_results = [self._build_results]

		def _initialize_thread(self, multiqueue, thread_index: int):
			if self._build_results is None:
				raise AttributeError("Cannot initialize thread unless graph has been built")
			self.multiqueue_requested_stop = lambda: multiqueue.coord.should_stop()

			self._thread = threading.Thread(target=self._on_start,
											args=[thread_index, self] + list(self._build_results))
			self.already_started = False

		@property
		def is_graph_built(self):
			return self._build_results is not None

		def start_safely(self, multiqueue, thread_index):
			if self._build_results is None:
				raise AttributeError("Cannot start thread until the graph has been built!")
			if not self._thread or self.already_started:
				self._initialize_thread(multiqueue, thread_index)
			self.already_started = True
			self._thread.start()

	def __init__(self, queues=None, scope_name: str = None):
		self.sess = None
		self.queues = []
		if queues is not None:
			self.add_queues(queues)
		self.thread_handlers = []
		self.coord = tf.train.Coordinator()
		super(HeteroThreadHeteroQueue, self).__init__(scope_name)

	def __getitem__(self, item):
		return self.queues[item]

	def set_session(self, session):
		self.sess = session

	def add_queue(self, queue: Queue):
		self.queues.append(queue)

	def add_queues(self, queues: [Queue]):
		for queue in queues:
			self.add_queue(queue)

	def add_thread_from_callback(self, thread_callback, *args, **kwargs):
		""" Creates a new thread initialized with the specified thread_callback and arbitrary arguments
		:param thread_callback: Tuple consisting of two callbacks:
			 - on_build_graph(multiqueue: AsyncMultiQueue, thread_index, *args, **kwargs) -> built_ops
			 - def on_start(multiqueue: AsyncMultiQueue, thread_index, thread_handler, built_ops) -> None
		:param args: Any arbitrary arguments to be additionally passed into the thread callback/s
		:return: None
		"""
		on_build_graph, on_start = thread_callback
		self.thread_handlers.append(
			HeteroThreadHeteroQueue.ThreadHandler(on_build_graph, on_start, *args, **kwargs))

	def add_threads_from_callback(self, thread_callback, num_threads: int, *args, **kwargs):
		""" Helper function that calls add_thread_from_callback @num_threads times with the specified arguments
		:param thread_callback:
		:param num_threads:
		:param args:
		:return:
		"""
		for thread_index in range(num_threads):
			self.add_thread_from_callback(thread_callback, *args, **kwargs)

	def build_unbuilt_graphs(self):
		with tf.name_scope(self.scope.abs + '/'):
			for thread_index, handler in enumerate(self.thread_handlers):
				if not handler.is_graph_built:
					handler.build_graph(thread_index)

	def start(self, index: int = None) -> None:
		"""Starts either a single thread specified by index or all threads if not specified
		:param index: [None, int] Index of thread to start or None to start all (default None)
		:return: None
		"""
		if self.sess is None:
			raise AttributeError("Cannot start threads unless session is set")
		if index is not None:
			self.thread_handlers[index].start_safely(self, index)
		else:
			self.build_unbuilt_graphs()
			if self.sess is None:
				raise AttributeError("Cannot start threads unless session is set")
			for i in range(self.thread_count):
				self.start(i)

	def stop(self, thread_index, queue_index, thread_stop_mode: str = 'stop', queue_stop_mode: str = 'close',
			 timeout_secs=10):
		"""Stop the specified thread using thread_stop_mode and with specified queue_stop_mode action on the specified queue_index
		:param thread_index: The specified thread to stop
		:param queue_index: [None, int] queue_index used by thread or None if the thread has already released it's queue (Cannot be None if queue_stop_mode has been specified)
		:param thread_stop_mode: 'stop' to preserve the thread handler if you wish to later start the same thread again or 'destroy' to stop and then pop the handler or None to leave the thread running (assumes the specified queue is not locked by any thread)
		:param queue_stop_mode: 'clear' to deque from the queue until empty or 'close' to close it permanently and cancel any pending requests or None if the thread will voluntarily release the resource itself
		:param timeout_secs:
		:return:
		"""
		# TODO Introduce code reusability with stop_all
		if thread_stop_mode in HeteroThreadHeteroQueue.THREAD_STOP_MODES:  # Assumes the thread does not hold a lock on the specified queue resource and so can remain running
			assert thread_index < len(self.thread_handlers)
			handler = self.thread_handlers[thread_index]
			handler.request_stop()
		if queue_stop_mode is not None:
			if self.sess is None:
				raise ValueError("Stop cannot include the closing of queues without specification of the session")

			assert queue_index < len(self.queues)
			queue = self.queues[queue_index]
			# All queue teardown modes encourage the thread to go blue whale ;)
			if queue_stop_mode == 'close':
				close_op = queue.close(cancel_pending_enqueues=True)
				self.sess.run(close_op, options=tf.RunOptions(timeout_in_ms=int(timeout_secs * 1000)))
			elif queue_stop_mode.startswith('clear'):
				# TODO Replicate similar logic for stop_all
				q_size = queue.size()
				deque_remaining_op = queue.dequeue_many(q_size)
				while self.sess.run(q_size):
					self.sess.run(deque_remaining_op, options=tf.RunOptions(timeout_in_ms=int(timeout_secs * 1000)))
			else:
				raise ValueError("Unknown queue teardown mode")

		if thread_stop_mode in HeteroThreadHeteroQueue.THREAD_STOP_MODES:
			try:
				# noinspection PyUnboundLocalVariable
				handler.thread.join(timeout_secs)
			except RuntimeError as e:
				# At this point the thread must have died or else it isn't safe to proceed to raise an exception
				for handler in self.thread_handlers:
					if handler.thread.is_alive():
						raise RuntimeError("Failed to join on the thread that refuses to die."
										   " Unsafe to proceed with multiqueue") from e
			handler.clear_stop()
			if thread_stop_mode == 'destroy':
				self.thread_handlers.pop(thread_index)

	# TODO What if user stops the threads, but later wants to destroy them before adding new threads
	def stop_all(self, thread_stop_mode: str = 'stop', queue_stop_mode: str = 'close', timeout_secs=None):
		assert thread_stop_mode is not None
		if thread_stop_mode in HeteroThreadHeteroQueue.THREAD_STOP_MODES:
			self.coord.request_stop()
		if queue_stop_mode is not None:
			if self.sess is None:
				raise ValueError("Stop cannot include the closing of queues without specification of the session")

			# All queue teardown modes encourage the thread to go blue whale ;)
			if queue_stop_mode == 'close' or queue_stop_mode == 'destroy':
				queue_teardown_ops = [queue.close(cancel_pending_enqueues=True) for queue in self.queues]
				timeout_ms = int(timeout_secs * 1000) if timeout_secs is not None else None
				self.sess.run(queue_teardown_ops, options=tf.RunOptions(timeout_in_ms=timeout_ms))
				if queue_stop_mode == 'destroy':
					self.queues.clear()
			elif queue_stop_mode.startswith('clear'):
				while True:
					# queue_teardown_ops = [queue.dequeue_many(queue.size()) for queue in self.queues if
					# 					  self.sess.run(queue.size())]
					queue_teardown_ops = [queue.dequeue() for queue in self.queues if
										  self.sess.run(queue.size())]
					if len(queue_teardown_ops) == 0:
						break
					self.sess.run(queue_teardown_ops, options=tf.RunOptions(timeout_in_ms=int(timeout_secs * 1000)))
			else:
				raise ValueError("Unknown queue stop mode: %s" % queue_stop_mode)

			if thread_stop_mode in HeteroThreadHeteroQueue.THREAD_STOP_MODES:
				try:
					self.coord.join([handler.thread for handler in self.thread_handlers if handler.thread is not None],
									stop_grace_period_secs=120 if timeout_secs is None else timeout_secs)
				except RuntimeError as e:
					# At this point the thread must have died or else it isn't safe to proceed to raise an exception
					for handler in self.thread_handlers:
						if handler.thread.is_alive():
							raise RuntimeError(
								"Failed to join on the thread that refuses to die. Unsafe to proceed with multiqueue") \
								from e
				self.coord.clear_stop()

				if thread_stop_mode == 'destroy':
					self.thread_handlers.clear()

	def each_queue(self, callback):
		with tf.variable_scope(self.scope.abs, reuse=True):
			return [callback(queue, index) for index, queue in enumerate(self.queues)]

	@property
	def thread_count(self):
		return len(self.thread_handlers)


class HeteroThreadHomoQueue(HeteroThreadHeteroQueue):
	def __init__(self, queue_constructor, num_queues: int, scope_name: str = None):
		assert queue_constructor is not None
		assert num_queues is not None
		queues = [queue_constructor() for _ in range(num_queues)]
		super(HeteroThreadHomoQueue, self).__init__(queues, scope_name)


class HeteroThreadMonoQueue(HeteroThreadHeteroQueue):
	def __init__(self, queue: Queue, scope_name: str = None):
		super(HeteroThreadMonoQueue, self).__init__([queue], scope_name)

	@property
	def queue(self):
		return self.queues[0] if self.queues is not None else None


class HomoThreadHeteroQueue(HeteroThreadHeteroQueue):
	def __init__(self, queues: [Queue], num_threads: int, sess: tf.Session, callback, scope_name: str = None, *args,
				 **kwargs):
		assert queues is not None
		assert sess is not None and callback is not None
		if num_threads is None:
			num_threads = len(queues)
		super(HomoThreadHeteroQueue, self).__init__(queues, scope_name)
		self.set_session(sess)
		self.add_threads_from_callback(callback, num_threads, *args, **kwargs)


class HomoThreadHomoQueue(HomoThreadHeteroQueue):
	def __init__(self, queue_builder, num_queues: int, num_thread: int, sess: tf.Session, callback,
				 scope_name: str = None,
				 *args, **kwargs):
		self.setup_scope(scope_name)
		with tf.variable_scope(self.scope.rel):
			queues = [queue_builder() for _ in range(num_queues)]
		# self.setup_scope('Explorer_mark1')
		super(HomoThreadHomoQueue, self).__init__(queues, num_thread, sess, callback, scope_name, *args, **kwargs)


class HomoThreadMonoQueue(HomoThreadHeteroQueue):
	def __init__(self, queue: Queue, num_threads: int, sess: tf.Session, callback, scope_name: str = None, *args,
				 **kwargs):
		super(HomoThreadMonoQueue, self).__init__([queue], num_threads, sess, callback, scope_name, *args, **kwargs)

	@property
	def mono_queue(self):
		return self.queues[0] if self.queues is not None else None


def AsyncMultiQueueFactory(thread: str = 'homo', queue: str = 'mono'):
	return {
		'hetero': {
			'hetero': HeteroThreadHeteroQueue,
			'homo': HeteroThreadHomoQueue,
			'mono': HeteroThreadMonoQueue
		},
		'homo': {
			'hetero': HomoThreadHeteroQueue,
			'homo': HomoThreadHomoQueue,
			'mono': HomoThreadMonoQueue
		}
	}[thread][queue]
