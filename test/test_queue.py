import queue as q
import time
import unittest
from unittest import TestCase

import numpy as np
import tensorflow as tf

from .utils import get_data_producer, assert_consumption, flush_qs, run_consumtion_asserter, run_consumtion
from .. import utils as U
from ..queue import HeteroThreadHeteroQueue, TfFIFOQueue, AsyncMultiQueueFactory, StagingAreaQueue, \
	HomoThreadHeteroQueue

ENQUEUE_MANY_SIZE = 3
RANDOM_STATE = np.random.randint(0, 100)
NUM_THREADS = 2


class TestHeteroQueueMethods(TestCase):
	def setUp(self):
		super().setUp()
		self.multiqueue = self.sess = self.dequeue_ops = None

	def tearDown(self):
		super().tearDown()

	def perform_default_setup(self):
		print("Building default graph")
		multiqueue = HeteroThreadHeteroQueue([
			TfFIFOQueue(capacity=2, dtypes=[tf.int32, tf.float32], shapes=[[3, 4], [2, 3]])])
		multiqueue.add_queue(
			TfFIFOQueue(capacity=2, dtypes=[tf.float32, tf.int32], shapes=[[1, 2], [3, 4]]))
		self._validation_qs = [q.Queue(), q.Queue()]
		multiqueue.add_threads_from_callback(
			(TestHeteroQueueMethods.on_build_default_graph, TestHeteroQueueMethods.on_default_start), 4,
			'default_multiqueue',
			self, RANDOM_STATE, self._validation_qs)
		dequeue_ops = [queue.dequeue() for queue in multiqueue.queues]
		sess = tf.Session()
		multiqueue.set_session(sess)
		self.multiqueue, self.sess, self.dequeue_ops = (multiqueue, sess, dequeue_ops)

	def perform_default_teardown(self):
		self.multiqueue.stop_all(queue_stop_mode='close')
		self.sess.close()

	@staticmethod
	def get_data_producer(thread_index, shapes, dtypes, enqueue_size=None, count=1000, random_state=1234):
		r = np.random.RandomState(random_state)
		for _ in range(count):
			values = []
			for shape, dtype in zip(shapes, dtypes):
				if enqueue_size is None:
					cnt = 1
				else:
					cnt = enqueue_size
				value = []
				for i in range(cnt):
					if dtype == tf.int32:
						value.append(r.randint(thread_index * 1000, (thread_index + 1) * 1000, shape.as_list()))
					elif dtype == tf.float32:
						value.append(r.rand(*shape.as_list()) * thread_index)
					else:
						raise ValueError("Unknown dtype encountered: ", dtype)
				values.append(np.array(value))
			if enqueue_size is None:
				yield [value[0] for value in values]
			else:
				yield values

	def run_default_consumtion_asserter(self, n_deque):
		for dequeu_i in range(n_deque):
			print("Dequeue %d:" % dequeu_i)
			all_dequeued = self.sess.run(self.dequeue_ops)
			validation_deques = [validation_q.get() for validation_q in self._validation_qs]
			assert_consumption(self, all_dequeued, validation_deques, verbose=False)

	@staticmethod
	def on_build_default_graph(thread_index, thread_handler, multiqueue: HeteroThreadHeteroQueue, test_case,
							   my_random_argument,
							   validation_qs):
		assert test_case is not None
		test_case.assertEqual(my_random_argument, RANDOM_STATE, "Incorrect custom argument sent to callback")
		test_case.assertTrue(validation_qs is not None)
		queue = multiqueue.queues[thread_index % 2]
		is_many = thread_index < (len(multiqueue.thread_handlers) / 2)
		placeholders = []
		for shape, dtype in zip(queue.shapes, queue.dtypes):
			enqueue_shape = [ENQUEUE_MANY_SIZE] + shape.as_list() if is_many else shape
			placeholders.append(tf.placeholder(dtype, enqueue_shape))
		enqueue_or_many_op = queue.enqueue_many(placeholders) if is_many else queue.enqueue(placeholders)
		return multiqueue.sess, queue, is_many, queue.size(), U.function(placeholders,
																		 enqueue_or_many_op), validation_qs

	@staticmethod
	def on_default_start(thread_index, thread_handler, sess: tf.Session, queue: TfFIFOQueue, is_enqueue_many,
						 q_size_op,
						 enqueue_or_many_op,
						 validation_qs):
		print("Thread %d: Starting" % thread_index)
		validation_q = validation_qs[thread_index % 2]
		shapes = queue.shapes
		dtypes = queue.dtypes
		with sess.as_default():
			many_producer = get_data_producer(thread_index, shapes, dtypes,
											  ENQUEUE_MANY_SIZE, random_state=RANDOM_STATE)
			producer = get_data_producer(thread_index, shapes, dtypes,
										 None, random_state=RANDOM_STATE)
			while not thread_handler.should_stop():
				time.sleep(np.random.uniform(0.0, 0.01))
				try:
					if thread_handler.should_stop():
						break
					q_size = sess.run(q_size_op)
					print("Thread %d(Queue%d size %d): %s" % (
						thread_index, thread_index % 2, q_size, "EnqueueMany" if is_enqueue_many else "Enqueue"))
					values = many_producer.__next__() if is_enqueue_many else producer.__next__()
					if thread_handler.should_stop():
						break
					if is_enqueue_many:
						for value in zip(*values):
							validation_q.put(value, block=True)
					else:
						validation_q.put(values)
					enqueue_or_many_op(*values)
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
			print("Thread %d: Exiting on coordinator request" % thread_index)

	def test_start_all_threads(self):
		self.perform_default_setup()
		multiqueue, sess, dequeue_ops = (self.multiqueue, self.sess, self.dequeue_ops)
		multiqueue.start()
		self.run_default_consumtion_asserter(n_deque=4)
		self.perform_default_teardown()

	def test_restart_all(self):
		self.perform_default_setup()
		multiqueue, sess, dequeue_ops = (self.multiqueue, self.sess, self.dequeue_ops)
		multiqueue.start()
		self.run_default_consumtion_asserter(n_deque=4)

		print('Stopping pipe before restart')
		multiqueue.stop_all(queue_stop_mode='clear_force', timeout_secs=4)
		flush_qs(self._validation_qs)

		# dequeue_ops = [queue.dequeue() for queue in multiqueue.queues]
		multiqueue.start()
		self.run_default_consumtion_asserter(n_deque=4)

		print('Stopping pipe')
		multiqueue.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(self._validation_qs)
		self.perform_default_teardown()

	def test_restart_some(self):
		self.perform_default_setup()
		multiqueue, sess, dequeue_ops = (self.multiqueue, self.sess, self.dequeue_ops)
		multiqueue.start()
		self.run_default_consumtion_asserter(n_deque=4)

		print('Stopping pipe before restart')
		multiqueue.stop(2, 2, queue_stop_mode=None, timeout_secs=4)
		multiqueue.start(2)
		self.run_default_consumtion_asserter(n_deque=4)

		print('Stopping pipe')
		multiqueue.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(self._validation_qs)
		self.perform_default_teardown()


class TestAsyncHomoQueues(TestCase):
	def setUp(self):
		super(TestAsyncHomoQueues, self).setUp()

	@staticmethod
	def on_build_default_graph(thread_index, thread_handler, multiqueue: HomoThreadHeteroQueue, sess: tf.Session,
							   test_case,
							   my_random_argument,
							   validation_qs):
		assert test_case is not None
		queue = multiqueue.queues[thread_index % len(multiqueue.queues)]
		test_case.assertEqual(my_random_argument, RANDOM_STATE, "Incorrect custom argument sent to callback")
		test_case.assertTrue(validation_qs is not None)
		validation_q = validation_qs[thread_index % len(multiqueue.queues)]
		is_many = False  # thread_index < int(NUM_THREADS / 2)
		placeholders = []
		for shape, dtype in zip(queue.shapes, queue.dtypes):
			enqueue_shape = [ENQUEUE_MANY_SIZE] + shape.as_list() if is_many else shape
			placeholders.append(tf.placeholder(dtype, enqueue_shape))
		enqueue_or_many_op = queue.enqueue_many(placeholders) if is_many else queue.enqueue(placeholders)
		return sess, queue, queue.size(), U.function(placeholders, enqueue_or_many_op), validation_q

	@staticmethod
	def on_default_start(thread_index, thread_handler, sess: tf.Session, queue, q_size_op, enqueue_or_many_op,
						 validation_q):
		print("Thread %d: Starting" % thread_index)
		shapes = queue.shapes
		dtypes = queue.dtypes
		with sess.as_default():
			many_producer = get_data_producer(thread_index, shapes, dtypes,
											  ENQUEUE_MANY_SIZE, random_state=RANDOM_STATE)
			producer = get_data_producer(thread_index, shapes, dtypes,
										 None, random_state=RANDOM_STATE)
			while not thread_handler.should_stop():
				time.sleep(np.random.uniform(0.0, 0.01))
				try:
					is_enqueue_many = False  # thread_index < int(ENQUEUE_MANY_SIZE / 2)
					if thread_handler.should_stop():
						break

					values = many_producer.__next__() if is_enqueue_many else producer.__next__()
					if thread_handler.should_stop():
						break
					if is_enqueue_many:
						for value in zip(*values):
							validation_q.put(value, block=True)
					else:
						validation_q.put(values)
					enqueue_or_many_op(*values)
					q_size = sess.run(q_size_op)
					print("Thread %d(Queue%d size %d): %s" % (
						thread_index, thread_index, q_size, "EnqueueMany" if is_enqueue_many else "Enqueue"))
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
			print("Thread %d: Exiting on coordinator request" % thread_index)

	def test_homo_hetero(self):
		validation_qs = [q.Queue() for _ in range(NUM_THREADS)]
		qs = [
			TfFIFOQueue(capacity=2, dtypes=[tf.int32, tf.float32], shapes=[[3, 4], [2, 3]]
						) for _ in
			range(NUM_THREADS)
		]
		sess = tf.Session()
		homo_hetero = AsyncMultiQueueFactory(thread='homo', queue='hetero')(
			qs, NUM_THREADS, sess,
			(TestAsyncHomoQueues.on_build_default_graph,
			 TestAsyncHomoQueues.on_default_start),
			sess, self, RANDOM_STATE, validation_qs
		)
		dequeue_ops = homo_hetero.each_queue(lambda queue, _: queue.dequeue())
		homo_hetero.start()

		run_consumtion_asserter(self, lambda: sess.run(dequeue_ops), validation_qs, n_deque=4)

		print('Stopping pipe before restart')
		homo_hetero.stop_all(queue_stop_mode='clear', timeout_secs=4)
		homo_hetero.start()
		run_consumtion_asserter(self, lambda: sess.run(dequeue_ops), validation_qs, n_deque=4)

		print('Stopping pipe')
		homo_hetero.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(validation_qs)

	def test_homo_homo(self):
		num_queues = NUM_THREADS
		validation_qs = [q.Queue() for _ in range(num_queues)]
		sess = tf.Session()
		homo_homo = AsyncMultiQueueFactory(thread='homo', queue='homo')(
			lambda: TfFIFOQueue(capacity=2, dtypes=[tf.int32, tf.float32], shapes=[[3, 4], [2, 3]]),
			num_queues, NUM_THREADS, sess,
			(TestAsyncHomoQueues.on_build_default_graph,
			 TestAsyncHomoQueues.on_default_start),
			sess, self, RANDOM_STATE, validation_qs
		)
		dequeue_ops = homo_homo.each_queue(lambda queue, _: queue.dequeue())
		homo_homo.start()

		print('Stopping pipe before restart')
		homo_homo.stop_all(queue_stop_mode='clear', timeout_secs=4)
		homo_homo.start()
		run_consumtion_asserter(self, lambda: sess.run(dequeue_ops), validation_qs, n_deque=4)

		print('Stopping pipe')
		homo_homo.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(validation_qs)

	def test_homo_mono(self):
		num_queues = 1
		validation_qs = [q.Queue() for _ in range(num_queues)]
		sess = tf.Session()
		homo_homo = AsyncMultiQueueFactory(thread='homo', queue='mono')(
			TfFIFOQueue(capacity=2, dtypes=[tf.int32, tf.float32], shapes=[[3, 4], [2, 3]]
						), NUM_THREADS, sess,
			(TestAsyncHomoQueues.on_build_default_graph,
			 TestAsyncHomoQueues.on_default_start),
			sess, self, RANDOM_STATE, validation_qs
		)
		dequeue_op = homo_homo.mono_queue.dequeue()
		homo_homo.start()

		print('Stopping pipe before restart')
		homo_homo.stop_all(queue_stop_mode='clear', timeout_secs=4)
		homo_homo.start()
		run_consumtion_asserter(self, lambda: sess.run([dequeue_op]), validation_qs, n_deque=4)

		print('Stopping pipe')
		homo_homo.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(validation_qs)

	def test_homo_homo_staging_q(self):
		validation_qs = [q.Queue() for _ in range(NUM_THREADS)]
		qs = [
			StagingAreaQueue(dtypes=[tf.float32, tf.int32], shapes=[[1], [1]], capacity=10
							 ) for _ in
			range(NUM_THREADS)
		]
		sess = tf.Session()
		homo_hetero = AsyncMultiQueueFactory(thread='homo', queue='hetero')(
			qs, NUM_THREADS, sess,
			(TestAsyncHomoQueues.on_build_default_graph,
			 TestAsyncHomoQueues.on_default_start),
			sess, self, RANDOM_STATE, validation_qs
		)

		dequeue_ops = homo_hetero.each_queue(lambda queue, _: queue.dequeue())
		homo_hetero.start()

		# Staging area does not guarantee order so don't assert
		values, _ = run_consumtion(self, lambda: sess.run(dequeue_ops), validation_qs, n_deque=4)
		print(values)

		print('Stopping pipe before restart')
		homo_hetero.stop_all(queue_stop_mode='clear', timeout_secs=4)
		homo_hetero.start()
		run_consumtion(self, lambda: sess.run(dequeue_ops), validation_qs, n_deque=4)

		print('Stopping pipe')
		homo_hetero.stop_all(queue_stop_mode='close', timeout_secs=4)
		flush_qs(validation_qs)

	def tearDown(self):
		super(TestAsyncHomoQueues, self).tearDown()


if __name__ == '__main__':
	unittest.main()
