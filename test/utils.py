import numpy as np
import tensorflow as tf


def run_consumtion(tester, dequeue_all_callback, validation_qs, n_deque):
	for dequeu_i in range(n_deque):
		print("Dequeue %d:" % dequeu_i)
		all_dequeued = dequeue_all_callback()
		validation_deques = [validation_q.get() for validation_q in validation_qs]
		return all_dequeued, validation_deques


def run_consumtion_asserter(tester, dequeue_all_callback, validation_qs, n_deque):
	for dequeu_i in range(n_deque):
		print("Dequeue %d:" % dequeu_i)
		all_dequeued = dequeue_all_callback()
		validation_deques = [validation_q.get() for validation_q in validation_qs]
		assert_consumption(tester, all_dequeued, validation_deques, verbose=False)


def assert_consumption(asserter, dequeues, validation_deques, verbose: bool):
	if verbose:
		print("Deque shapes:")
	asserter.assertEqual(len(dequeues), len(validation_deques), "Mismatch in number of queues")
	for i, dequeued in enumerate(dequeues):
		if verbose:
			print("Queue %d: " % i, end="")
			print("(%d shapes)" % len(dequeued), end="")
			print([np.shape(element) for element in dequeued])
		for dequeue, valid_dequeue in zip(dequeues, validation_deques):
			for elem, valid_elem in zip(dequeue, valid_dequeue):
				assert np.shape(elem) == np.shape(valid_elem)
				reduced = np.isclose(elem, valid_elem, rtol=1e-3, atol=1e-6)
				while len(list(np.shape(reduced))) > 0:
					reduced = np.logical_and.reduce(reduced)
				if not reduced:
					print(elem, valid_elem)
				# TODO Simply use locks to prevent such false alarms
				asserter.assertTrue(reduced,
									"Deque results aren't close enough to be disregarded as calculation imprecision "
									"(This may be a false alarm due to a known bug "
									"with the queues not guaranteeing order, "
									"try rerunning the test case)")


def get_data_producer(thread_index, shapes, dtypes, enqueue_size=None, count=1000, random_state=1234):
	r = np.random.RandomState(random_state)
	for _ in range(count):
		values = []
		for shape, dtype in zip(shapes, dtypes):
			if enqueue_size is None:
				cnt = 1
			else:
				cnt = enqueue_size
			value = []
			for i in range(cnt):
				if dtype == tf.int32:
					value.append(r.randint(thread_index * 1000, (thread_index + 1) * 1000, shape.as_list()))
				elif dtype == tf.float32:
					value.append(r.rand(*shape.as_list()) * thread_index)
				else:
					raise ValueError("Unknown dtype encountered: ", dtype)
			values.append(np.array(value))
		if enqueue_size is None:
			yield [value[0] for value in values]
		else:
			yield values


def flush_qs(validation_qs):
	for valid_q in validation_qs:
		while not valid_q.empty():
			valid_q.get()
