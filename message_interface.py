from queue import Queue
from typing import NewType

import tensorflow as tf


class MsgIHandle(object):
	"""Message handle interface.
	Each thread maintains an instance of this class in order for MsgCoordinatorI to communicate
	"""

	class Request:
		pass

	MESSAGE_STOP_REQUEST = Request()
	MESSAGE_PROCEED_REQUEST = Request()

	def __init__(self):
		self._queue = Queue(maxsize=1)

	def request_barrier_stop(self, request_timeout=None):
		self._queue.put(MsgIHandle.MESSAGE_STOP_REQUEST, timeout=request_timeout)

	def request_barrier_proceed(self, request_timeout=None):
		self._queue.put(MsgIHandle.MESSAGE_PROCEED_REQUEST, timeout=request_timeout)

	def barrier(self, receive_timeout=None):
		self._get_stop_request(receive_timeout)
		self._get_proceed_request(receive_timeout)

	def _get_stop_request(self, receive_timeout=None):
		result = self._queue.get(block=True, timeout=receive_timeout)
		self._queue.task_done()
		if not (isinstance(result, MsgIHandle.Request) and result == MsgIHandle.MESSAGE_STOP_REQUEST):
			raise AssertionError("Expected stop request")

	def _get_proceed_request(self, receive_timeout=None):
		result = self._queue.get(block=True)
		self._queue.task_done()
		if not (
					isinstance(result,
							   MsgIHandle.Request) and result == MsgIHandle.MESSAGE_PROCEED_REQUEST):
			raise AssertionError("Expected proceed request")

	def has_message(self):
		return not self._queue.empty()

	# TODO Allow support for non-blocking put
	def deliver_message(self, msg, override_pending=False, timeout=None):
		if self._queue.full() and override_pending:
			self._queue.get()  # TODO Fix potential race condition: queue is filled by another thread before our 'put'
		self._queue.put(msg, timeout=timeout)

	def pickup_message(self, synchronous=False, timeout=None):
		try:
			self._get_stop_request()
		except AssertionError as e:
			raise AssertionError("Ensure corresponding message propagation is synchronous") from e

		result = self._queue.get(timeout=timeout)
		if isinstance(result, MsgIHandle.Request):
			raise AssertionError("Expecting message, instead found MessageInterface.Request")

		try:
			self._get_proceed_request()
		except AssertionError as e:
			raise AssertionError("Synchronous message propagations must be followed by proceed commands") from e
		return result


# noinspection PyPep8Naming
MessageInterfaceType = NewType('MsgIHandle', MsgIHandle)


class MsgCoordinatorI(object):
	"""Message Coordinator Interface
	This coordinator coordinates and communicates messages with the MsgIHandle of each thread it coordinates/
	"""

	def __init__(self, *args):
		super(MsgCoordinatorI, self).__init__(*args)
		assert self.thread_handlers is not None
		assert self.thread_count is not None
		for thread_handler in self.thread_handlers:
			thread_handler.message_interface = MsgIHandle()

	def add_thread_from_callback(self, thread_callback, *args):
		super(MsgCoordinatorI, self).add_thread_from_callback(thread_callback, *args)
		self.thread_handlers[-1].message_interface = MsgIHandle()

	def _get_destinations(self, destinations: [list, None]):
		if destinations is None:
			destinations = range(self.thread_count)
		return destinations

	def _get_messages(self, msgs: [list, None], destinations):
		if not isinstance(msgs, list):
			msg = msgs
			msgs = [msg for _ in destinations]
		assert len(msgs) == len(destinations)
		return msgs

	def propagate_messages(self, msgs: [list, None], destinations: [list, None] = None, delivery_timeout: int = None,
						   sync=False, sync_timeout: int = None,
						   override_pending=False):
		destinations = self._get_destinations(destinations)
		msgs = self._get_messages(msgs, destinations)

		if sync:
			self.request_stop(destinations, sync_timeout)

		for dst, msg in zip(destinations, msgs):
			MessageInterfaceType(self.thread_handlers[dst].message_interface) \
				.deliver_message(msg, override_pending=override_pending, timeout=delivery_timeout)

		if sync:
			self.request_proceed(destinations, sync_timeout)

	def collect_messages(self, destinations: [list, None] = None, pickup_timeout: int = None,
						 synchronous=False, sync_timeout: int = None):
		destinations = self._get_destinations(destinations)

		if synchronous:
			self.request_stop(destinations, sync_timeout)

		for dst in destinations:
			MessageInterfaceType(self.thread_handlers[dst].message_interface) \
				.pickup_message(synchronous, timeout=pickup_timeout)

		if synchronous:
			self.request_proceed(destinations, sync_timeout)

	def request_stop(self, destinations=None, sync_timeout: float = None):
		if destinations is None:
			destinations = range(self.thread_count)
		for dst in destinations:
			thread_handler = self.thread_handlers[dst]
			try:
				if thread_handler._thread and thread_handler._thread.is_alive():
					MessageInterfaceType(thread_handler.message_interface) \
						.request_barrier_stop(sync_timeout)
			except tf.errors.CancelledError as e:
				print(e)

	def request_proceed(self, destinations=None, sync_timeout: float = None):
		if destinations is None:
			destinations = range(self.thread_count)
		for dst in destinations:
			thread_handler = self.thread_handlers[dst]
			try:
				if thread_handler._thread and thread_handler._thread.is_alive():
					MessageInterfaceType(thread_handler.message_interface) \
						.request_barrier_proceed(sync_timeout)
			except tf.errors.CancelledError as e:
				print(e)
