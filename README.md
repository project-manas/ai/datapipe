Description
===
Create utilities for convenient and flexible creation of pipelines that support asynchronous interation between data producers and consumers.  
###Status:
 - Extended functionality still under development
 - Work well exclusively with tensorflow but designed to be easily integrated with other libraries
 - Recommended to be used as a Git submodule under any project to encourage open-source contribution.

###AsyncMultiQueue
Class that coordinates M Queues and N Threads that operate on them.  
Note that this class makes minimal assumptions about how the threads interact with the queues and each other for maximum flexibility.  
Thus, for simple policies it is recommended to use one of the other provided classes that abstract the use of this class in the background.

####Features:
 - Custom callbacks that have their own logic to operate on those queues.
 - Any thread can do both enqueue and dequeue on any queue
 - Fine control of starting and stopping threads at runtime
 - Threads can either be stopped and restarted later or destroyed and released
 - Queues associated with stopped threads can either be cleared or closed permanently and released.
 - Built test cases for most functionality of queue
 - (TODO) Add support for having queue on GPU with tf StagingArea

###